import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import {Headers, Http, RequestOptionsArgs} from "@angular/http";
import * as firebase from 'firebase/app'
@Injectable({
  providedIn: 'root'
})
export class DataService {
  repoResult: any;
  currentRepo: string;
  currentCommit$: Observable<any>;
  token: any;
  
  headers: Headers = new Headers();
  private requestOptions: RequestOptionsArgs = {};
  private apiServer: string = "https://api.github.com";
  argToken = '';
  user: any;

  constructor(private http: Http, private route: Router, private afAuth: AngularFireAuth) {
    this.headers.set("Content-Type", "application/json");
    this.requestOptions.headers = this.headers;
    this.route.navigate(['/search']) // per riportare alla pagina d'ingresso ad ogni refresh, anche se l'url che si sta refreshando non corrisponde a /search
  }
  // ```
  getRepo(val): Observable<any> {
    return this.get(`/repos/${val}/branches${this.argToken}`).pipe(
      map(response => {
        this.currentRepo = val;
        this.repoResult = response.json();
        return this.repoResult
      }),
      take(1)
    )
    // https://api.github.com/repos/<user>/<repo>/branches
  }
  getCommitInfo(val): Observable<any> {
    this.currentCommit$ = this.get(`/repos/${this.currentRepo}/commits/${val}${this.argToken}`).pipe(
      map(response => response.json())      
    )
    return this.currentCommit$;
  }
  login () {
    const provider = new firebase.auth.GithubAuthProvider();
    provider.addScope('repo');
    this.afAuth.auth.signInWithPopup(provider).then((result) => {
      // This gives you a GitHub Access Token. You can use it to access the GitHub API.
      this.token = result.credential.accessToken;
      this.argToken = `?access_token=${this.token}`;
      // The signed-in user info.
      this.user = result.user;
      console.log(result.credential)
    }).catch((error) => {
      console.error(error)
    });
    // RIGUARDO CURL, POCO CI HO CAPITO. ANYWAY I GOT FIREBASE IN MY HANDS!
    /*const body = {
      email: 'kristian050398@gmail.com',
      user_password: '',
      token: ''
    }
    this.GitHub.post(, body,).subscribe(res => console.log(res.json))*/
  }

  getRequestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
    this.requestOptions.headers = this.headers;
    if (options) {
        Object.assign(options, this.requestOptions);
    }
    return this.requestOptions;
  }

  get(endPoint: string, options?: RequestOptionsArgs): Observable<any> {
      return this.http.get(this.createUrl(endPoint), this.getRequestOptions(options));
  }

  createUrl(endPoint): string {

      let url = this.apiServer + endPoint;
      if (!endPoint.startsWith('/')) {
          url = this.apiServer + '/' + endPoint;
      }
      return url;
  }
}
