import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  repoResult: any;
  currentCommit$: Observable<any>;
  currentRepo: any;
  searchString: string;
  constructor(public dataSer: DataService, private route: Router) { }
  // ````````````````````````
  ngOnInit() {
  }
  login() {
    this.dataSer.login();
  }
  getRepo(val) { // gli torna la lista dei commit
    this.dataSer.getRepo(val).subscribe(res => {
      if (res) { // se request alla repository ha prodotto risultati
        this.currentRepo = val;
        this.route.navigate(['/commit']);
      }
    })
  }
}
