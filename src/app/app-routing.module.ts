import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { CommitContainerComponent } from './commit-container/commit-container.component';

const routes: Routes = [
  { path: 'search', component: SearchBarComponent },
  { path: 'commit', component: CommitContainerComponent },
  { path: '', redirectTo: '/search', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
