import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-commit-list',
  templateUrl: './commit-list.component.html',
  styleUrls: ['./commit-list.component.css']
})
export class CommitListComponent implements OnInit {
  @Input() repoResult: any;
  selectedCommit: string;
  constructor(private dataSer: DataService) { }

  ngOnInit() {
  }
  getCommitInfo(val: string) {
    this.selectedCommit = val;
    this.dataSer.getCommitInfo(val);
  }

}
