import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from 'angularfire2';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { CommitListComponent } from './commit-list/commit-list.component';
import { CommitDetailsComponent } from './commit-details/commit-details.component';
import { CommitContainerComponent } from './commit-container/commit-container.component';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {HttpModule} from "@angular/http";
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { FormatFromJsonPipe } from './format-from-json.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    CommitListComponent,
    CommitDetailsComponent,
    CommitContainerComponent,
    FormatFromJsonPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    AngularFireModule.initializeApp(environment.config),
    AngularFireAuthModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
