import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-commit-container',
  templateUrl: './commit-container.component.html',
  styleUrls: ['./commit-container.component.css']
})
export class CommitContainerComponent implements OnInit {

  constructor(public dataSer: DataService, private route: Router) { }

  ngOnInit() {
  }
  backToSearch() {
    this.route.navigate(['/search'])
  }
}
