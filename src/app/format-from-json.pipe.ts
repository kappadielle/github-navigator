import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatFromJson'
})
export class FormatFromJsonPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let string = value.toString();
    string = string.replace(/{|}|"/g, () => {
      return '';
    })
    string = string.substring(0, string.indexOf('comment_count') - 2)
    if (string == 'null') {
      string = 'Sto caricando...';
    }
    return string;
  }

}
